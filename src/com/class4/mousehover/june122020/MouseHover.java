package com.class4.mousehover.june122020;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class MouseHover {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");

		driver = new ChromeDriver();
	/*	driver.get("https://opensource-demo.orangehrmlive.com/");
		Thread.sleep(3000);


		driver.manage().window().maximize();
		System.out.println("browser maximized");
		driver.findElement(By.cssSelector("input#txtUsername")).sendKeys("Admin");
		Thread.sleep(1000);
		driver.findElement(By.name("txtPassword")).sendKeys("admin123");
		Thread.sleep(1000);
		driver.findElement(By.id("btnLogin")).click();
		Thread.sleep(1000);
		Actions a=new Actions(driver);
		WebElement admin=driver.findElement(By.xpath("//b[contains(text(),'Admin')]"));
		a.moveToElement(admin).build().perform();
		Thread.sleep(5000);
		System.out.println("Admin Mousehover successfully");
		WebElement qq=driver.findElement(By.xpath("//a[contains(text(),'Qualifications')]"));
		a.moveToElement(qq).build().perform();
		Thread.sleep(5000);
		System.out.println("Qualifications Mousehover successfully");
		WebElement mm=driver.findElement(By.xpath("//a[contains(text(),'Memberships')]"));
		a.moveToElement(mm).build().perform();
		Thread.sleep(5000);
		System.out.println("Membership Mousehover successfully");
		//Drag And Drop
		driver.get("https://www.globalsqa.com/demo-site/draganddrop/");
		driver.manage().window().maximize();
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@class,'demo-frame')]")));
		Thread.sleep(5000);
        System.out.println("Switch To frame");
        
        WebElement to=driver.findElement(By.xpath("//ul[@id=\"gallery\"]/li[1]"));
        Thread.sleep(5000);
        Actions a1=new Actions(driver);
        WebElement from=driver.findElement(By.xpath("//div[@id='trash']"));
        Thread.sleep(15000);
        a1.dragAndDrop(from,to).build().perform();
        System.out.println("The Required Drag And Drop Action is Done");*/
        driver.get("https://jqueryui.com/droppable/");
        Thread.sleep(3000);
        driver.manage().window().maximize();
        Thread.sleep(2000);
        Actions ac=new Actions(driver);
        driver.switchTo().frame(0);
        WebElement to1=driver.findElement(By.xpath("//div[@id='draggable']"));
        WebElement from1=driver.findElement(By.xpath("//div[@id='droppable']"));
        Thread.sleep(5000);
        Point point=to1.getLocation();
        int xcord=point.getX();
        int ycord=point.getY();
        System.out.println(xcord+"  "+ycord);
        ac.dragAndDropBy(from1,146,18).perform();
        
        
        
        
      //  ac.dragAndDrop(from1,to1).build().perform();
        
        System.out.println("The Required Drag And Drop Action is Done");
        driver.quit();


	}

}
