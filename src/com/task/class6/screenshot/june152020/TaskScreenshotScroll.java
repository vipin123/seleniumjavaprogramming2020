package com.task.class6.screenshot.june152020;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TaskScreenshotScroll {
	static int a=0;
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.edureka.co/");
		System.out.println("Launch the Edureka Website");
		driver.manage().window().maximize();
		System.out.println("Maximize the window");
		Thread.sleep(5000);
		WebElement to=driver.findElement(By.xpath("//h2[contains(text(),'Trending Courses')]"));
		Point location=to.getLocation();
		int y=location.getY();
		System.out.println("Location of Y co-ordinate is : "+y);
		Thread.sleep(5000);
	    TakesScreenshot t=(TakesScreenshot)driver;
		File sourse=t.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(sourse , new File("./Screenshots/Screen["+a+"].png"));
		a++;
		System.out.println("First Screenshot Captured");
		//It's Scroll Down
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,604)");
		System.out.println("Scroll Down");
		Thread.sleep(5000);
		FileUtils.copyFile(sourse, new File("./Screenshots/Screen["+a+"].png"));
		a++;
		System.out.println("2nd Screenshot Captured");
		//For Scroll Up
		js.executeScript("window.scrollBy(0,-604)");
		System.out.println("Scroll Up");
	    //End of web page
	    js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
	    System.out.println("End of Web Page");
	    System.out.println("Perfect 100%");
	    Thread.sleep(3000);
		FileUtils.copyFile(sourse, new File("./Screenshots/Screen["+a+"].png"));
		a++;
		System.out.println("3Rd Screenshot Captured");
		

		
		
		
		driver.quit();

	}

}
