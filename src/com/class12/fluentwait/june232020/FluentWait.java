package com.class12.fluentwait.june232020;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
public class FluentWait {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",".\\driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://www.facebook.com/");//"https://www.facebook.com/"
		System.out.println("Facebook is Launched");
		Thread.sleep(5000);
		String currentUrl=driver.getCurrentUrl();
		System.out.println("The Current Url is :=>"+currentUrl);
        System.out.println("=========================================");
		String title=driver.getTitle();
		System.out.println("The Given Title is :=>" +title);
        System.out.println("=========================================");
        driver.manage().window().maximize();
        System.out.println("Maximize the Browser");
        
      
        Wait fw= new FluentWait().withTimeout(30,TimeUnit.SECONDS).pollingEvery(5,TimeUnit.SECONDS).ignoring(NoSuchElementException.class,TimeoutException.class);
        fw.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("email"))));
        
        try {
        	driver.findElement(By.id("email")).sendKeys("dsg");
        	
        }catch(Exception e) {
        	System.out.println("Using fluent wait it has been checked");
        	
        }
        
        driver.findElement(By.id("pass")).sendKeys("sdfg");
        driver.quit();
        System.out.println("quit the browser");
        

	}

	private org.openqa.selenium.support.ui.FluentWait<WebDriver> withTimeout(int i, TimeUnit seconds) {
		// TODO Auto-generated method stub
		return null;
	}
	

	}
