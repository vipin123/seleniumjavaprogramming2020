package com.class3.locator.orange.hrm;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocatorClass3 {

	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",".\\driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com/");
		System.out.println("ORANGE HRM is Launched");
		String title=driver.getTitle();
		System.out.println(title);
		driver.manage().window().maximize();
		Thread.sleep(5000);
		System.out.println("Browser Maximized");
		driver.findElement(By.cssSelector("input#txtUsername")).sendKeys("Admin ");
		Thread.sleep(1000);

		driver.findElement(By.cssSelector("input#txtPassword")).sendKeys("admin123");
		Thread.sleep(1000);

		driver.findElement(By.id("btnLogin")).click();
		
		Thread.sleep(15000);
		

		System.out.println("Clicked on Login");
		System.out.println("View Home Page of ORANGE HRM");
		String text=driver.findElement(By.className("panelTrigger")).getText();
		System.out.println(text);
		driver.findElement(By.linkText("Welcome Admin")).click();
		driver.findElement(By.partialLinkText("Welcome")).click();

		System.out.println("Welcome Admin Click succesfully");
		driver.quit();
		System.out.println("WELCOME HOME PAGE OF ORANGE HRM");

}}
