package com.class10.webtablehanding.june192020;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTableHandling {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/test/web-table-element.php");
		Thread.sleep(3000);
		System.out.println("Launch guru99.com web-table-element url");
		driver.manage().window().maximize();
		System.out.println("Maximize the browser");
		Thread.sleep(5000);
		System.out.println("Wait for 5 seconds");
		WebDriver f=driver.switchTo().frame("flow_close_btn_iframe");
		System.out.println("Switching to the frame :" +f);
		WebElement cl=driver.findElement(By.id("closeBtn"));
		cl.click();
		System.out.println("Click on the close button :=>" +cl);
		WebDriver d=driver.switchTo().defaultContent();
		System.out.println("Switches to the default content :=>" +d);
		List<WebElement> col=driver.findElements(By.xpath("//table[@class='dataTable']//tr//th"));
		System.out.println("The List for the given column :=> " +col);
		System.out.println("===================================================");

		for (int i = 0; i < col.size(); i++) {
			System.out.println(col.get(i).getText());
			
		}
		System.out.println("===================================================");

		Thread.sleep(5000);
		List<WebElement> row=driver.findElements(By.xpath("//table[@class='dataTable']//tbody//tr//td[1]"));
		System.out.println(row.size());
		Thread.sleep(5000);
		System.out.println("===================================================");
		List<WebElement> all=driver.findElements(By.xpath("//table[@class='dataTable']//tbody//tr//td"));
		System.out.println(all.size());
		Thread.sleep(5000);
		System.out.println("===================================================");
		for (int i=1;i<=row.size();i++) {
			for (int j =1; j < col.size(); j++) {
				WebElement element=driver.findElement(By.xpath("//table[@class='dataTable']//tbody//tr["+i+"]//td["+j+"]"));
				
				System.out.println(element.getText());
				
				
			}
			
		}


		
		driver.quit();
		System.out.println("Quit The Browser");
		System.out.println("==============The End=================");

	}

}
