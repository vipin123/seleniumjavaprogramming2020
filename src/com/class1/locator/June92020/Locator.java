package com.class1.locator.June92020;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Locator {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",".\\driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://www.facebook.com/");
		System.out.println("Facebook is Launched");
		String title=driver.getTitle();
		System.out.println(title);
		driver.manage().window().maximize();
		Thread.sleep(5000);
		System.out.println("Browser Maximized");
		driver.findElement(By.id("email")).sendKeys("xyz");
		Thread.sleep(1000);
		driver.findElement(By.id("pass")).sendKeys("xyz1");
		Thread.sleep(1000);
		driver.findElement(By.id("u_0_b")).click();
		System.out.println("Clicked on Login");

  
		driver.quit();
		
	}

}
