package com.class6.screenshot.june152020;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Screenshot {

	public static void main(String[] args) throws InterruptedException, IOException {
		int c=2;
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.edureka.co/");
		System.out.println("Launch the edureka Website");
		driver.manage().window().maximize();
		System.out.println("Maximize the window");
		
		WebElement to=driver.findElement(By.xpath("//h2[contains(text(),'Recent Additions')]"));
		Point location=to.getLocation();
		int y=location.getY();
		System.out.println("The Location of y is : "+y);
	    Thread.sleep(3000);
	    TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(source, new File("./Screenshots/Screen["+c+"].png"));
		c++;
		System.out.println("2nd Screenshot Captured");
	    //It is for scroll down
	    JavascriptExecutor js=(JavascriptExecutor)driver;
	    js.executeScript("window.scrollBy(0,2064)");
	    System.out.println("Scroll Down");
	    Thread.sleep(5000);
		FileUtils.copyFile(source, new File("./Screenshots/Screen["+c+"].png"));
		c++;
		System.out.println("3rd Screenshot Captured");

        //It is for scroll up
	    js.executeScript("window.scrollBy(0,-2064)");
	    System.out.println("Scroll Up");
	    //End of web page
	    js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
	    System.out.println("End of Web Page");
	    System.out.println("Perfect 100%");
	    Thread.sleep(3000);
		FileUtils.copyFile(source, new File("./Screenshots/Screen["+c+"].png"));
		c++;
		System.out.println("4th Screenshot Captured");
        driver.quit();
				

	}

	public static void sshot(WebDriver driver) {
		
	}

}
