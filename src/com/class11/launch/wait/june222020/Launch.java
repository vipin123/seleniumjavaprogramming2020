package com.class11.launch.wait.june222020;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Launch {
	public static WebDriver driver; 
	
	public Launch(String baseurl) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",".\\driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get(baseurl);//"https://www.facebook.com/"
		System.out.println("Facebook is Launched");
		Thread.sleep(5000);
		String currentUrl=driver.getCurrentUrl();
		System.out.println("The Current Url is :=>"+currentUrl);
        System.out.println("=========================================");
		String title=driver.getTitle();
		System.out.println("The Given Title is :=>" +title);
        System.out.println("=========================================");
        driver.manage().window().maximize();
        System.out.println("Maximize the Browser");

		
		
		
	}

	public Launch() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws InterruptedException {
		Launch l=new Launch("https://www.facebook.com/");
		System.out.println("=================================");
		driver.quit();
		System.out.println("Quit the Browser");
        System.out.println("The End");
	}

}
