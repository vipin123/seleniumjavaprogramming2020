package com.class5.task.robot.june132020;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class TaskClickAndHold {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.get("https://google.com");
		System.out.println("Click And Hold URL is Launched");
		Thread.sleep(3000);
		driver.manage().window().maximize();
		System.out.println("Maximize the browser");
		WebElement serbtn=driver.findElement(By.xpath("//a[contains(text(),'Sign in')]"));
		Actions a=new Actions(driver);
		a.clickAndHold(serbtn).build().perform();
		System.out.println("Click and hold action is performed");
		Thread.sleep(6000);

		driver.quit();
		System.out.println("QUIT THE BROWSER.0 0");
		

	}

}
