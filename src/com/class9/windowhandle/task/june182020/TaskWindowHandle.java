package com.class9.windowhandle.task.june182020;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TaskWindowHandle {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://learn.letskodeit.com/p/practice");
		Thread.sleep(3000);
		System.out.println("Launch learn.letskodeit.com pop up url");
		driver.manage().window().maximize();
		System.out.println("Maximize the browser");
		WebElement ot=driver.findElement(By.xpath("//a[contains(text(),'Open Tab')]"));
		ot.click();
		System.out.println("The tab got clicked is :"+ot);
		Thread.sleep(8000);
		String parentwindow=driver.getWindowHandle();
		Set<String> windowhandle=driver.getWindowHandles();
		for (String childwindow : windowhandle) {
			if(!parentwindow.equals(childwindow)) {
				driver.switchTo().window(childwindow);
				System.out.println("Switches to the child window");
				WebElement sr=driver.findElement(By.xpath("//input[@class='form-control search input-lg']"));
				sr.sendKeys("Python");
				System.out.println("In serch box Python is typed :"+sr);
				Thread.sleep(8000);
				WebElement sr1=driver.findElement(By.xpath("//input[@class='form-control search input-lg']"));
				sr1.click();
				System.out.println("Click on search box button :"+sr1);
				Thread.sleep(8000);
				driver.close();
				System.out.println("Close the browser");
				

			}
			
		}
		String pr=driver.switchTo().window(parentwindow).getTitle();
		System.out.println("The window of the parent title is :"+pr);
		String text=driver.findElement(By.xpath("//a[contains(text(),'Open Tab')]")).getText();
		System.out.println("The text of the given String is :"+text);
		
		/*WebElement sr=driver.findElement(By.xpath("//input[@class='form-control search input-lg']"));
		sr.sendKeys("");*/
		driver.quit();
		System.out.println("Quit the browser");

	}

}
