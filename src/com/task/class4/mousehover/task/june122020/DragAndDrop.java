package com.task.class4.mousehover.task.june122020;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragAndDrop {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://the-internet.herokuapp.com/drag_and_drop");
        System.out.println("The URL is Launched");
        Thread.sleep(5000);
        driver.manage().window().maximize();
        System.out.println("Maximize the Browser");
        Actions act=new Actions(driver);
        WebElement to=driver.findElement(By.id("column-a"));
        WebElement from =driver.findElement(By.id("column-b"));
        act.dragAndDrop(to,from).build().perform();
        Thread.sleep(5000);
        System.out.println("Drag and Drop action is performed");
        driver.quit();
        System.out.println("Quit the Browser");
        
        

	}

}
