package com.class6.screenshot.june152020;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ScreenshotsJsExecutor {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver",".\\driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com/");
		System.out.println(" ORANGE HRM is Launched");
		driver.manage().window().maximize();
		System.out.println("browser maximized");
		TakesScreenshot obj=(TakesScreenshot) driver;
		File ss=obj.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(ss, new File ("./screenshot/screen.png"));
		
		driver.findElement(By.cssSelector("input#txtUsername")).sendKeys("Admin");
		Thread.sleep(1000);
		driver.findElement(By.name("txtPassword")).sendKeys("admin123");
		Thread.sleep(1000);
		driver.findElement(By.id("btnLogin")).click();
		Thread.sleep(1000);
		FileUtils.copyFile(ss, new File ("./screenshot/screen1.png"));

		driver.quit();

	}

}
