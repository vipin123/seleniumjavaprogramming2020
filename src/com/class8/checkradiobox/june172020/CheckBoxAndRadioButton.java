package com.class8.checkradiobox.june172020;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CheckBoxAndRadioButton {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/test/radio.html");
		Thread.sleep(5000);
		System.out.println("Launch guru99.com radio url");
		driver.manage().window().maximize();
		System.out.println("Maximize the browser");
		driver.findElement(By.id("vfb-7-1")).click();
		System.out.println("Radio button with option 1 is selected");
		Thread.sleep(5000);
		driver.findElement(By.id("vfb-7-3")).click();
		System.out.println("Radio button with option 3 is selected");
		Thread.sleep(5000);
		WebElement ck1=driver.findElement(By.id("vfb-6-0"));
		ck1.click();
		Thread.sleep(5000);
		System.out.println("The Checkbox option first is :"+ck1);
		WebElement ck2=driver.findElement(By.id("vfb-6-1"));
		ck2.click();
		Thread.sleep(5000);
		System.out.println("The Checkbox option second is :"+ck2);


		WebElement ck3=driver.findElement(By.id("vfb-6-0"));
		ck3.click();
		Thread.sleep(5000);
		System.out.println("The Checkbox option third is :"+ck3);
		driver.quit();
		System.out.println("Quit the browser");






		

	}

}
