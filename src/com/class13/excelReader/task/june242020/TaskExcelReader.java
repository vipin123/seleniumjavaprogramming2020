package com.class13.excelReader.task.june242020;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TaskExcelReader {

	public static void main(String[] args) throws IOException {
		FileInputStream f1=new FileInputStream("./data/Demo Automation1.xlsx");
		XSSFWorkbook wb =new XSSFWorkbook(f1);
		XSSFSheet sheet=wb.getSheetAt(0);
		int rowcount=sheet.getLastRowNum();
		System.out.println("The Given Row Count is => "+rowcount);
		System.out.println("===================================");
		int colcount=sheet.getRow(0).getLastCellNum();
		System.out.println("The given Column Count is => "+colcount);
		System.out.println("===================================");
		XSSFCell cell=sheet.getRow(0).getCell(0);
		String result=cell.getStringCellValue();
		System.out.println("The Given XSSFCell Value is => "+result);
		System.out.println("===================================");
		XSSFCell cell1=sheet.getRow(2).getCell(19);
		String result1=cell.getStringCellValue();
		System.out.println("The Given Result of XSSFCell value is => "+result1);
		System.out.println("===================================");
		for (int i = 0; i<=rowcount ; i++) {
			for (int j = 0; j <colcount; j++) {
				XSSFCell cell2=sheet.getRow(i).getCell(j);
				String finalResult=cell2.getStringCellValue();
				System.out.println("The Final Result of Excel Reader is => "+finalResult);
				
				
			}
			System.out.println();
			
		}

		
		

		
		System.out.println("===============The End============================");

	}

}
