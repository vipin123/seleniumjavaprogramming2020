package com.class7.iframe.june162020;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Iframe {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://jqueryui.com/droppable/");
		Thread.sleep(3000);
		System.out.println("Launch the jqueryui.com url");
		driver.manage().window().maximize();
		System.out.println("Maximize the browser");
		Thread.sleep(3000);
		Actions a=new Actions(driver);
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@class='demo-frame']")));
		System.out.println("Found the Iframe");
		WebElement to=driver.findElement(By.cssSelector("#draggable"));
		Point location=to.getLocation();
		int y=location.getY();
		System.out.println("The Location of y is : "+y);
		 JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,153)");
		System.out.println("Scroll Down");
		WebElement from=driver.findElement(By.cssSelector("#draggable"));
		WebElement to1=driver.findElement(By.cssSelector("#droppable"));
		a.dragAndDrop(from,to1).build().perform();
		driver.quit();
		System.out.println("Quit the browser");
		

		

	}

}
