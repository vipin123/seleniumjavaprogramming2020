package com.class11.launch.wait.task.june222020;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Grofers  {
	public static WebDriver driver;
	

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",".\\driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://grofers.com/");//"https://www.facebook.com/"
		System.out.println("Grofers application is Launched");
		Thread.sleep(5000);
		String currentUrl=driver.getCurrentUrl();
		System.out.println("The Current Url is :=>"+currentUrl);
        System.out.println("=========================================");
		String title=driver.getTitle();
		System.out.println("The Given Title is :=>" +title);
        System.out.println("=========================================");
        driver.manage().window().maximize();
        System.out.println("Maximize the Browser");
        System.out.println("====================================");
        WebDriverWait gr=new WebDriverWait(driver,20);
        gr.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("input.react-autosuggest__input"))));
        driver.findElement(By.cssSelector("input.react-autosuggest__input")).sendKeys("Atta");
        
        try {
        	gr.until(ExpectedConditions.urlContains("grofers"));
        	System.out.println("Match Found");
        	
        }catch(Exception e){
        	System.out.println("Match Not Found");

        	
        }
        driver.quit();
        System.out.println("Quit the Browser");
        System.out.println("=======The End===============");
		

	}

}
