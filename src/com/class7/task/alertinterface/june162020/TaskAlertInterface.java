package com.class7.task.alertinterface.june162020;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TaskAlertInterface {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.testandquiz.com/selenium/testing.html");
		System.out.println("Test and Quiz app is Launche");
		driver.manage().window().maximize();
		System.out.println("Maximize the browser");
		Thread.sleep(3000);
		WebElement w=driver.findElement(By.xpath("//button[contains(text(),'Generate Alert Box')]"));
		Point location=w.getLocation();
		int y=location.getY();
		Thread.sleep(5000);
		System.out.println("The Location of y is : "+y);
		JavascriptExecutor js=(JavascriptExecutor)driver;
	    js.executeScript("window.scrollBy(0,658)");
	    System.out.println("Scroll Down");
	    w.click();
	    Alert o=driver.switchTo().alert();
	    String text=o.getText();
	    System.out.println("The given text of alert is "+text);
	    Thread.sleep(5000);
	    o.accept();
	    System.out.println("Alert Aceepted");
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//button[contains(text(),'Generate Confirm Box')]")).click();
	    Alert cb=driver.switchTo().alert();
	    cb.dismiss();
	    System.out.println("Alert is :"+cb);
	    System.out.println("Alert is dismiss");
	    driver.quit();
		System.out.println("Quit the browser");

	}

}
