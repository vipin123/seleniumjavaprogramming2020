package com.class8.select.june172020;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectMethod {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/test/newtours/register.php");
		Thread.sleep(3000);
		System.out.println("Launch newtours.com url");
		driver.manage().window().maximize();
		System.out.println("Maximize the browser");
		driver.switchTo().frame("flow_close_btn_iframe");
		System.out.println("Iframe found");
		driver.findElement(By.xpath("//div[@id='closeBtn']")).click();
		System.out.println("Close the  button");
		driver.switchTo().defaultContent();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		System.out.println("Scroll down the bottom of the webpage");
		Thread.sleep(5000);

		//Select country=new Select(driver.findElement(By.tagName("select")));
		Select country=new Select(driver.findElement(By.xpath("//select[@name='country']")));

		List<WebElement> opt=country.getOptions();
		int size=opt.size();
		for(int i =0; i<size ; i++)
		{
		String sValue = opt. get(i). getText();
		System. out. println(sValue);
		}
		System.out.println("No of values in Drop Down : "+size);
		country.selectByVisibleText("ANGOLA");
		System.out.println("Visible Selection through text is done");
		
		country.selectByIndex(12);
		System.out.println("Visible Selection through index is done");
		
		country.selectByValue("AMERICAN SAMOA");
		System.out.println("Visible Selection through value is done");


		driver.quit();
		System.out.println("Quit the browser");

	}

}
