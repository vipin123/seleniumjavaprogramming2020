package com.class3.locator.task.june102020;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocatorClassTask3 {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",".\\driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("http://demowebshop.tricentis.com/");
		System.out.println("DEMO WEB SHOP app is Launched");
		String title=driver.getTitle();
		System.out.println(title);
		driver.manage().window().maximize();
		Thread.sleep(5000);
		System.out.println("Browser Maximized");
		//Click on Login button
		driver.findElement(By.xpath("//a[contains(text(),'Log in')]")).click();
		System.out.println("Clicked on Login");
		Thread.sleep(5000);

		driver.findElement(By.cssSelector("input#Email")).sendKeys("vipintekade032@gmail.com");
		driver.findElement(By.cssSelector("input#Password")).sendKeys("REODEO2016");
        driver.findElement(By.xpath("//input[1][@type='submit']")).click();
        Thread.sleep(5000);
        System.out.println("Enter into DEMO WEB SHOP");
        String text=driver.findElement(By.xpath("//a[contains(text(),'vipintekade032@gmail.com')]")).getText();
        System.out.println(text);
        driver.findElement(By.xpath("//a[contains(text(),'Log out')]")).click();
        Thread.sleep(5000);
        System.out.println("Logout DEMO WEB SHOP app");
         driver.quit();
        System.out.println("Quit the browser");
        
	}

}
