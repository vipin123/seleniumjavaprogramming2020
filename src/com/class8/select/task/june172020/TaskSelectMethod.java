package com.class8.select.task.june172020;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TaskSelectMethod {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.facebook.com/");
		Thread.sleep(3000);
		System.out.println("Launch facebook.com url");
		driver.manage().window().maximize();
		System.out.println("Maximize the browser");
		Select day=new Select(driver.findElement(By.id("day")));
		Thread.sleep(5000);
		day.selectByIndex(16);
		System.out.println("Selecting the date through visible Index is done");
		Thread.sleep(5000);
		day.selectByValue("10");
		System.out.println("Selecting the date through value is done");
		Thread.sleep(5000);
		day.selectByVisibleText("8");
		System.out.println("Selecting the date through visible text is done");
		List<WebElement> opt=day.getOptions();
		int size=opt.size();
		for(int i =0; i<size ; i++)
		{
		String sValue = opt. get(i). getText();
		System. out. println(sValue);
		}
		System.out.println("No of day values in Drop Down : "+size);
		driver.quit();
		System.out.println("Quit the browser");


	}

}
