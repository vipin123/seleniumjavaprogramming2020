package com.class13.excelReader.june242020;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {

	public static void main(String[] args) throws IOException  {
		FileInputStream f=new FileInputStream("./data/TestData.xlsx");
		XSSFWorkbook wb=new XSSFWorkbook(f);
		XSSFSheet sheet=wb.getSheetAt(0);
		int rowcount=sheet.getLastRowNum();
        System.out.println("The Given Row Count is =>" +rowcount);
		System.out.println("===================================");

        int colcount=sheet.getRow(0).getLastCellNum();
        System.out.println("The Given Column Count is =>" +colcount);
		System.out.println("===================================");

        XSSFCell cell=sheet.getRow(0).getCell(0);
        String result=cell.getStringCellValue();
        System.out.println("The Given XSSFCell value is =>:"+result);
		System.out.println("===================================");

        XSSFCell cell2=sheet.getRow(5).getCell(2);
        String result1=cell2.getStringCellValue();
        System.out.println("The Given XSSF Cell Value is => "+result1);
		System.out.println("===================================");

        
        for (int i = 0; i<=rowcount; i++) {
        	for (int j = 0; j <colcount ; j++) {
        		XSSFCell cell3=sheet.getRow(i).getCell(j);
        		String cellvalue3=cell3.getStringCellValue();
        		System.out.println("The entire sheet value is => :"+cellvalue3);
                System.out.println("========The End===============");

				
			}
        		System.out.println();
			
		}
        

        

	}

}
