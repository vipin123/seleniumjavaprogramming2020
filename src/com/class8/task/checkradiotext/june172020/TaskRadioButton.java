package com.class8.task.checkradiotext.june172020;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TaskRadioButton {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://testautomationpractice.blogspot.com/");
		Thread.sleep(5000);
		System.out.println("Launch testautomationpractice.com url");
		driver.manage().window().maximize();
		System.out.println("Maximize the browser");
		 driver.switchTo().frame(driver.findElement(By.xpath("//*[@id=\"frame-one1434677811\"]")));
		System.out.println("Iframe found ");
		//It is for scroll down
		JavascriptExecutor js=(JavascriptExecutor)driver;
	    js.executeScript("window.scrollBy(0,611)");
	    System.out.println("Scroll Down");
		
		//WebElement m=driver.findElement(By.cssSelector("input#RESULT_RadioButton-7_0"));
		driver.findElement(By.cssSelector("input#RESULT_RadioButton-7_0")).click();
		System.out.println("Radio button of male gets selected :");
		//m.click();
		/*Point location=m.getLocation();
		int y=location.getY();
		System.out.println("The Location of y is : "+y);
		Thread.sleep(5000);*/
		 //It is for scroll down
	   /* JavascriptExecutor js=(JavascriptExecutor)driver;
	    js.executeScript("window.scrollBy(0,611)");
	    System.out.println("Scroll Down");
	    Thread.sleep(5000);*/
	   // m.click();
	    System.out.println("male Radio Button gets Selected");

	    WebElement f=driver.findElement(By.cssSelector("input#RESULT_RadioButton-7_1"));
		System.out.println("Radio button of female gets selected :"+f);
	    
	    Thread.sleep(5000);
        f.click();
	    System.out.println("Female Radio Button gets Selected");
		driver.quit();
		System.out.println("Quit the browser");

	}

}
