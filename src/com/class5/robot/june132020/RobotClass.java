package com.class5.robot.june132020;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class RobotClass {
	public static WebDriver driver;
	

	public static void main(String[] args) throws InterruptedException, AWTException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com/");
		System.out.println("ORANGE HRM URL is Launched");
		Thread.sleep(3000);
		driver.manage().window().maximize();
		System.out.println("Maximize the browser");
		Robot r=new Robot();
		Thread.sleep(500);
		driver.findElement(By.cssSelector("input#txtUsername")).sendKeys("Admin "+Keys.TAB);
		Thread.sleep(1000);
		//Robot robot = null;
		r.keyPress(KeyEvent.VK_TAB);
		Thread.sleep(1000);
		r.keyPress(KeyEvent.VK_A); 
		Thread.sleep(500);
		r.keyPress(KeyEvent.VK_D); 
		Thread.sleep(500); 
		r.keyPress(KeyEvent.VK_M); 
		Thread.sleep(500); 
		r.keyPress(KeyEvent.VK_I); 
		Thread.sleep(500); 
		r.keyPress(KeyEvent.VK_N); 
		Thread.sleep(500); 
		r.keyPress(KeyEvent.VK_1); 
		Thread.sleep(500); 
		r.keyPress(KeyEvent.VK_2); 
		Thread.sleep(500); 
		r.keyPress(KeyEvent.VK_3); 
		Thread.sleep(500); 
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		
		driver.quit();
		
		
		System.out.println("Quit the browser");


	}

}
