package com.class7.task.iframe.june162020;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TaskIframe {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/test/guru99home/");
		Thread.sleep(3000);
		System.out.println("Launch the guru99.com url");
		driver.manage().window().maximize();
		System.out.println("Maximize the browser");
		 driver.switchTo().frame("flow_close_btn_iframe");
		System.out.println("We found Iframe ");
		driver.findElement(By.xpath("//img[@src='https://live.sekindo.com/content/video/splayer/assets/placeHolder.png']")).click();
		System.out.println("We have clicked on the iframe image");
		Thread.sleep(3000);
		driver.quit();
		System.out.println("Quit the browser");

	}

}
