package com.task.ff.launch.june82020;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TaskFFLaunch {
	
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.gecko.driver",".\\driver\\geckodriver.exe");
        driver=new FirefoxDriver();
        driver.get("https://www.myntra.com/");
        System.out.println("Myntra Launched Successfully");
        String url=driver.getCurrentUrl();
        System.out.println(url);
        String title=driver.getTitle();
		System.out.println(title);
		driver.get("https://www.snapdeal.com/");
		String url1=driver.getCurrentUrl();
        System.out.println(url1);
        System.out.println("SnapDeal Launched ");
        String title1=driver.getTitle();
		System.out.println(title1);
		driver.manage().window().maximize();
		Thread.sleep(15000);
		System.out.println("Browser Maximized");
		driver.quit();
        
	}
	

}
