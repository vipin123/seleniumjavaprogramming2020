package com.class8.task.checkradiotext.june172020;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TaskRadioAndCheckBox {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://learn.letskodeit.com/p/practice");
		Thread.sleep(5000);
		System.out.println("Launch the learn.letskodeit.com Website");
		driver.manage().window().maximize();
		System.out.println("Maximize the window");
		Thread.sleep(5000);
		//driver.findElement(By.xpath("//input[@id='bmwradio']")).click();
		WebElement b=driver.findElement(By.xpath("//input[@id='bmwradio']"));
		b.click();
		System.out.println("Radio Button of BMW got Selected :"+b);
		Thread.sleep(5000);
		WebElement h=driver.findElement(By.xpath("//input[@id='hondaradio']"));
		h.click();
		System.out.println("Radio Button of Honda got Selected :"+h);
		
		System.out.println("-------------Check Box-----------------");
		WebElement b1=driver.findElement(By.xpath("//input[@id='bmwcheck']"));
		b1.click();
		System.out.println("Check Box for BMW is :"+b1);
		Thread.sleep(5000);
		WebElement bz=driver.findElement(By.xpath("//input[@id='benzcheck']"));
		bz.click();
		System.out.println("Check Box for Benz is :"+bz);
		Thread.sleep(5000);
		WebElement h1=driver.findElement(By.xpath("//input[@id='hondacheck']"));
		h1.click();
		System.out.println("Check Box for Honda is :"+h1);
		Thread.sleep(5000);
		driver.quit();
		System.out.println("Quit the browser");


	}

}
