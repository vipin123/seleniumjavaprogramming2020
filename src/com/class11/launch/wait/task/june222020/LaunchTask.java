package com.class11.launch.wait.task.june222020;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LaunchTask {
	public static WebDriver driver;
	
	public LaunchTask(String baseurl) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",".\\driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get(baseurl);//"https://www.snapdeal.com/"
		System.out.println("Snapdeal is Launched");
		Thread.sleep(3000);
		String currentUrl=driver.getCurrentUrl();
		System.out.println("The Current Url is :=>"+currentUrl);
        System.out.println("=========================================");
		String title=driver.getTitle();
		System.out.println("The Given Title is :=>" +title);
        System.out.println("=========================================");
        driver.manage().window().maximize();
        System.out.println("Maximize the Browser");
		
	}

	public static void main(String[] args) throws InterruptedException {
		LaunchTask lt=new LaunchTask("https://www.snapdeal.com/");
		
		System.out.println("========================");
		driver.quit();
		System.out.println("Quit the browser");
		System.out.println("=====THE END =============");

	}

}
