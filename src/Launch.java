import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Launch {
   public static WebDriver driver;
   
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",".\\driver\\chromedriver.exe");
        driver=new ChromeDriver();
        driver.get("https://www.facebook.com/?");
        driver.get("https://www.flipkart.com/?");
        System.out.println("fb Launched");
        String curUrl= driver.getCurrentUrl();
        System.out.println(curUrl);
        
        String title=driver.getTitle();
        System.out.println(title);
        driver.manage().window().maximize();
        Thread.sleep(15000);
        System.out.println("Browser Maximized");
        driver.quit();
	}

}
