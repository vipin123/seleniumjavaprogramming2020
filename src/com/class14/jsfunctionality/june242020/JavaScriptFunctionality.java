package com.class14.jsfunctionality.june242020;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.class11.launch.wait.june222020.Launch;

public class JavaScriptFunctionality extends Launch{

	public JavaScriptFunctionality() throws InterruptedException {
		super();
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws InterruptedException {
		JavaScriptFunctionality j=new JavaScriptFunctionality();
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("window.location=https://opensource-demo.orangehrmlive.com");
		WebElement username=driver.findElement(By.name("txtUsername"));
		WebElement pwd=driver.findElement(By.name("txtPassword"));
		WebElement submit=driver.findElement(By.name("Submit"));
		js.executeScript("arguments[0].value='"+"Admin"+"';",username);
		Thread.sleep(2000);
		js.executeScript("arguments[0].value='"+"admin123"+"';",pwd);
		Thread.sleep(2000);
		js.executeScript("arguments[0].click();",submit);
		

		driver.quit();
		System.out.println("Quit the browser");
		

	}

}
