package com.class5.robot.june132020;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ClickAndHold {
	public static WebDriver driver;


	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.get("https://selenium08.blogspot.com/2020/01/click-and-hold.html");
		System.out.println("Click And Hold URL is Launched");
		Thread.sleep(3000);
		driver.manage().window().maximize();
		System.out.println("Maximize the browser");
		WebElement titleB=driver.findElement(By.xpath("//li[@name='B']"));
		Actions a=new Actions(driver);
		a.contextClick(titleB).perform();
		Thread.sleep(3000);
		a.moveToElement(titleB);
		a.clickAndHold().perform();
		a.release().perform();
		System.out.println("Click and Hold Action is performed ");
		
		driver.quit();
		System.out.println("QUIT THE BROWSER");

	}

}
