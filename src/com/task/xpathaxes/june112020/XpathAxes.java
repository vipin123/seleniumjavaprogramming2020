package com.task.xpathaxes.june112020;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.ArrayList;

public class XpathAxes {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",".\\driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com/");
		System.out.println("ORANGE HRM is Launched");
		String title=driver.getTitle();
		System.out.println(title);
		driver.manage().window().maximize();
		Thread.sleep(5000);
		System.out.println("Browser Maximized");
		//Basic xpath
		/*driver.findElement(By.id("txtUsername")).sendKeys("Admin");
		driver.findElement(By.id("txtPassword")).sendKeys("admin123");
		System.out.println("Xpath id attribute is selected");*/
		//Contains

		String s1=driver.findElement(By.xpath("//a[contains(text(),'Forgot your password?')]")).getText();
		System.out.println("Xapth with contains function : "+s1);
		//driver.findElement(By.xpath("//span[@class='form-hint'][contains(text(),'Username')]")).sendKeys("Admin");
		//driver.findElement(By.xpath("//span[@class='form-hint'][contains(text(),'Password')]")).sendKeys("admin123");
		//System.out.println("Xpath contains attribute is selected");
		//4.Starts-with function
		String s2=driver.findElement(By.xpath("//a[starts-with(text(),'Forgot your password?')]")).getText();
		System.out.println("xpath with starts-with function  :  "+s2);
		//5.AND OR
        driver.findElement(By.xpath("//input[@name='txtUsername' and @id='txtUsername']")).sendKeys("Admin");
        driver.findElement(By.xpath("//input[@name='txtPassword' and @id='txtPassword']")).sendKeys("admin123");
        driver.findElement(By.xpath("//input[@id='btnLogin' and @name='Submit']")).click();
        Thread.sleep(5000);
        System.out.println("Home Page of Orange HRM is Displayed");
        //6.Following Syntax
        String s3=driver.findElement(By.xpath("//a[@href='http://www.orangehrm.com/']//following::a[@id='welcome']")).getText();
        System.out.println("The Following syntax is   :"+s3);
        //7.Ancestor
        String s4=driver.findElement(By.xpath("//a[@href='http://www.orangehrm.com/']//ancestor::div[1]")).getText();
        System.out.println("The Ancestor Syntax is :"+s4);
        //8:Child Syntax
         driver.findElements(By.xpath("//div[@id='MP_btn']//child::input"));
        System.out.println("The Child Synatx for WebElement is : Marketplace");
        //9.Preceding div
        driver.findElement(By.xpath("//span[contains(text(),'Assign Leave')]//preceding::img[4]"));//Assign Leave
        //10.Following Sibling
        //driver.findElement(By.xpath(""));
        //11.Parent
        driver.findElement(By.xpath("//a[@href='http://www.orangehrm.com/']//parent::div")).click();

        
		driver.quit();
		
		

	}

}
