package com.task.chrome.Launch.June82020;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TaskLaunch {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",".\\driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://www.snapdeal.com/");
		System.out.println("SnapDeal Launched");
		driver.get("https://www.amazon.in/");
		System.out.println("Amazon Launched");
		String url=driver.getCurrentUrl();
		System.out.println(url);
		String title=driver.getTitle();
		System.out.println(title);
		driver.manage().window().maximize();
		Thread.sleep(15000);
		System.out.println("Browser Maximized");
		driver.quit();
		
		

	}

}
