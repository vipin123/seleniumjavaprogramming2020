package com.task.class4.mousehover.task.june122020;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class TaskMouseHover {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://www.edureka.co/");
        System.out.println("The Url is Launched");
        Thread.sleep(5000);
        driver.manage().window().maximize();
        System.out.println("The browser is maximized");
        Actions a=new Actions(driver);
        WebElement cat=driver.findElement(By.xpath("//a[@class='dropdown-toggle hidden-xs hidden-sm ga_browse_top_cat']//span[contains(text(),'Categories')]"));
        a.moveToElement(cat).build().perform();
        Thread.sleep(5000);
        System.out.println("Catageory mouse hover done successfully");
        WebElement bg=driver.findElement(By.xpath("//a[@class='dropdown-toggle ga_top_category' and text()='Big Data']"));
        a.moveToElement(bg).build().perform();
        Thread.sleep(5000);
        driver.quit();
        
        
	}

}
