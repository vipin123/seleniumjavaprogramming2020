package com.class10.webtablehandling.task.june192020;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTableHandlingTask {
	public static WebDriver driver;
	//Tommrrow try with this url :https://learn.letskodeit.com/p/practice

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://only-testing-blog.blogspot.com/2014/05/form.html");
		Thread.sleep(3000);
		System.out.println("Launch only-testing-blog.blogspot.com url");
		driver.manage().window().maximize();
		System.out.println("Maximize the browser");
		Thread.sleep(3000);
		System.out.println("===================================================");

		List<WebElement> row=driver.findElements(By.xpath("//table//tbody/tr//td[1]"));
		System.out.println("The size of the row is :"+row);
		System.out.println("The size of the given row are is :=>");
		System.out.println(row.size());
		System.out.println("===================================================");
		List<WebElement> all=driver.findElements(By.xpath("//table//tbody/tr//td"));
		System.out.println("The size of the row is :"+all);
		System.out.println("The size of the given row are is :=>");
		System.out.println(all.size());
		System.out.println("===================================================");
      /* for (int i = 1; i < row.size(); i++) {
        	for (int j = 1; j < all.size(); j++) {
        		WebElement el=driver.findElement(By.xpath("//table//tbody//tr["+i+"]//td["+j+"]"));
    			System.out.println(el.getText());
				
			}
			
		}*/
		
		
		driver.quit();
		System.out.println("Quit the browser");


	}

}
